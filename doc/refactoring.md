# Refactoring

!!! note
    This page is a work in progress.

<!-- !!! quote "When to refactor"

    [Rule of Three](https://refactoring.guru/refactoring/when)

    - When you’re doing something for the first time, just get it done.
    - When you’re doing something similar for the second time, cringe at having to repeat but do the same thing anyway.
    - When you’re doing something for the third time, start refactoring.

## Commented code, dead code

## "code smells"

### Duplication

### Long functions

### Long parameter lists

### Global variables

### Nesting 

## Comments 

## Struture your code

### Minimize entries to the path

-->


